<div class="aside-nav">
    <ul>
        <?php if (isset($currentKey)): ?>
            <?php $keys = explode('.', $currentKey);  ?>
        <?php endif; ?>
        @if(isset($keys) && strlen($keys[0]))
            @foreach (\Illuminate\Support\Arr::get($menu->items, current($keys) . '.children') as $item)
                <li class="{{ $menu->getActive($item) }}">
                    <a href="{{ $item['url'] }}">
                        {{ trans($item['name']) }}

                        @if ($menu->getActive($item))
                            <!-- <i class="angle-right-icon"></i> -->
                        @endif
                    </a>
                </li>
            @endforeach
        @endif
    </ul>

</div>
