@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.sales.orders.title') }}
@stop

@section('content')
    <div class="content order-content">
        <div class="page-header">
            <div class="page-title">
                <h4>{{ __('admin::app.sales.orders.title') }}</h4>
            </div>
        </div>
        <div class="page-content">
            @inject('orderGrid', 'Webkul\Admin\DataGrids\OrderDataGrid')
        </div>
        {!! $orderGrid->render() !!}

    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>

@stop

@push('scripts')
    @include('admin::export.export', ['gridName' => $orderGrid])
@endpush
